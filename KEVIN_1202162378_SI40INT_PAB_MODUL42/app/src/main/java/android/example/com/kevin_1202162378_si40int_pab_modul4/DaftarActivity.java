package android.example.com.kevin_1202162378_si40int_pab_modul4;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class DaftarActivity extends AppCompatActivity {
    private TextView LoginAkun;
    private FirebaseAuth firebaseAuth;
    private EditText NamaLengkap, Email, Password;
    private Button Daftar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daftar);

        LoginAkun = (TextView) findViewById(R.id.textView7);
        LoginAkun.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(i);
            }
        });
        NamaLengkap = (EditText) findViewById(R.id.editText3);
        Email = (EditText) findViewById(R.id.editText4);
        Password = (EditText) findViewById(R.id.editText5);
        Daftar = (Button) findViewById(R.id.button2);

        firebaseAuth = FirebaseAuth.getInstance();
        FirebaseApp.initializeApp(DaftarActivity.this);

        Daftar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String namalengkap = NamaLengkap.getText().toString().trim();
                String email = Email.getText().toString().trim();
                String password = Password.getText().toString().trim();

                if (TextUtils.isEmpty(namalengkap)) {
                    Toast.makeText(DaftarActivity.this, "Enter Your Full name", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (TextUtils.isEmpty(email)) {
                    Toast.makeText(DaftarActivity.this, "Enter Your Email", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (TextUtils.isEmpty(password)) {
                    Toast.makeText(DaftarActivity.this, "Enter Your Password", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (namalengkap.length() < 6) {
                    Toast.makeText(DaftarActivity.this, "Full Name is Too Short", Toast.LENGTH_SHORT).show();
                }
                if (email.length() < 10) {
                    Toast.makeText(DaftarActivity.this, "Email is Too Short", Toast.LENGTH_SHORT).show();
                }
                if (password.length() < 6) {
                    Toast.makeText(DaftarActivity.this, "Password is Too Short", Toast.LENGTH_SHORT).show();
                }
                firebaseAuth.createUserWithEmailAndPassword(email, password)
                        .addOnCompleteListener(DaftarActivity.this, new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if (task.isSuccessful()) {

                                    startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                                    Toast.makeText(DaftarActivity.this, "Registration Complete", Toast.LENGTH_SHORT).show();

                                } else {

                                    Toast.makeText(DaftarActivity.this, "Authentication Failed", Toast.LENGTH_SHORT).show();
                                }

                            }
                        });

            }
        });
    }
}
